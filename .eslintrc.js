module.exports = {
  "extends": "airbnb",
  "parser": "babel-eslint",
  "env": {
    "browser": true
  },
  "rules": {
    "no-console":0,
    "jsx-a11y/label-has-for": [
      2,
      {
        "components": ["Label"],
        "required": {
          "every": ["id"]
        },
        "allowChildren": false,
      }
    ],

    "react/destructuring-assignment": ["error", {
      "functional": "always",
      "class": "never",
    }],

    "import/prefer-default-export": "off",
  }
};
