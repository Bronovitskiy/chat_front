const BASE_URL = 'http://localhost:3000/';

const LOCAL_STORAGE = {
  IS_LOGIN: 'auth_header',
};

const REG_EXP = {
  EMAIL: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  PASSWORD: /^[0-9a-zA-Z]{8,}$/,
};

const HTTP_METHODS = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
};


export {
  BASE_URL,
  LOCAL_STORAGE,
  REG_EXP,
  HTTP_METHODS,
};
