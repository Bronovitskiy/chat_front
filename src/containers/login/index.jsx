import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import LoginForm from '../../components/login';
import { FORM_NAMES } from './constants';
import { REG_EXP, HTTP_METHODS } from '../../config';
import request from '../../utils/request';


class Login extends Component {
  state = {
    email: '',
    password: '',
    confirmPassword: '',
    valid: {
      email: false,
      password: false,
      confirmPassword: false,
    },
    isLogin: false,
  }

  onSuccess = () => {
    this.setState({
      isLogin: true,
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const { valid } = this.state;
    const isValid = Object.values(valid).every(item => (item));
    if (isValid) {
      const params = {
        email: this.state.email,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
      };

      request('/auth/sign_in', HTTP_METHODS.POST, params)
        .then(this.onSuccess)
        .catch((error) => {
          console.log(`error: ${error}`);
        });
    }
  }

  handleInputChange = ({ target }) => {
    const value = target.type === 'checkbox' ? target.checked : target.value;

    const isValid = this.showInputError(target);

    this.setState(prevState => ({
      [target.id]: value,
      valid: {
        ...prevState.valid,
        [target.id]: isValid,
      },
    }));
  }

  showInputError = (target) => {
    let valid = true;
    const { password } = this.state;

    switch (target.id) {
      case FORM_NAMES.EMAIL:
        valid = REG_EXP.EMAIL.test(String(target.value).toLowerCase());
        break;

      case FORM_NAMES.PASSWORD:
        valid = REG_EXP.PASSWORD.test(String(target.value));
        break;

      case FORM_NAMES.CONFIRM_PASSWORD:
        valid = target.value === password;
        break;

      default:
    }

    return valid;
  }

  render() {
    const { isLogin } = this.state;
    if (isLogin) {
      return <Redirect to="/home" />;
    }

    return (
      <LoginForm
        handleSubmit={this.handleSubmit}
        handleInputChange={this.handleInputChange}
        user={this.state}
      />
    );
  }
}


export default Login;
