export const chartActions = {
  SET_CHATS: 'SET_CHATS',

  setChats: chats => ({
    type: chartActions.SET_CHATS,
    payload: chats,
  }),
};
