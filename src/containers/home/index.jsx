import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import request from '../../utils/request';
import { HTTP_METHODS } from '../../config';
import ChatList from '../../components/chat-list';
import { chartActions } from './actions';


class Home extends Component {
  componentDidMount() {
    request('/chats', HTTP_METHODS.GET, {})
      .then(this.onSuccess)
      .catch((error) => {
        console.log(`error: ${error}`);
      });
  }

  onSuccess = (data) => {
    this.props.setChats(data);
  }

  render() {
    return (
      <ChatList chats={this.props.chats} />
    );
  }
}

const mapStateToProps = state => ({
  chats: state.home.chats,
});

const mapDispatchToProps = {
  setChats: chartActions.setChats,
};

Home.propTypes = {
  chats: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setChats: PropTypes.func.isRequired,
};


export default connect(mapStateToProps, mapDispatchToProps)(Home);
