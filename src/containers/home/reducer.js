import { chartActions } from './actions';

const initialState = {
  chats: [],
};

export default function homeReducer(state = initialState, { type, payload }) {
  switch (type) {
    case chartActions.SET_CHATS:
      return {
        ...state,
        chats: payload,
      };
    default:
      return state;
  }
}
