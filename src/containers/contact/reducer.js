import { contactsActions } from './actions';

const initialState = {
  contacts: [],
};

export default function contactReducer(state = initialState, { type, payload }) {
  switch (type) {
    case contactsActions.SET_CONTACTS:
      return {
        ...state,
        contacts: payload,
      };
    default:
      return state;
  }
}
