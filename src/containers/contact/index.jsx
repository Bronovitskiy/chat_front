import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import request from '../../utils/request';
import { HTTP_METHODS } from '../../config';
import ContactList from '../../components/contact';
import { contactsActions } from './actions';

class Contact extends Component {
  componentDidMount() {
    request('/contacts', HTTP_METHODS.GET, {})
      .then(this.onSuccess)
      .catch((error) => {
        console.log(`error: ${error}`);
      });
  }

  onSuccess = (data) => {
    this.props.setContacts(data);
  }

  render() {
    return (
      <ContactList contacts={this.props.contacts} />
    );
  }
}

const mapStateToProps = state => ({
  contacts: state.contact.contacts,
});

const mapDispatchToProps = {
  setContacts: contactsActions.setContacts,
};

Contact.propTypes = {
  contacts: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  setContacts: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
