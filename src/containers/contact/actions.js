export const contactsActions = {
  SET_CONTACTS: 'SET_CONTACTS',

  setContacts: contacts => ({
    type: contactsActions.SET_CONTACTS,
    payload: contacts,
  }),
};
