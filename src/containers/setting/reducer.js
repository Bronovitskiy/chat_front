import { settingActions } from './actions';

const initialState = {
  id: 0,
  status: '',
  noDisturb: false,
  isSortChat: false,
};

export default function settingReducer(state = initialState, { type, payload }) {
  switch (type) {
    case settingActions.UPDATE_SETTING:
      return {
        ...state,
        [payload.key]: payload.value,
      };
    case settingActions.SET_SETTING:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
}
