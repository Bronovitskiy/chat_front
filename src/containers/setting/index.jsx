import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SettingForm from '../../components/setting';
import { HTTP_METHODS } from '../../config';
import request from '../../utils/request';
import { settingActions } from './actions';


class Setting extends Component {
  componentDidMount() {
    request('/settings', HTTP_METHODS.GET, {})
      .then(this.onSuccess)
      .catch((error) => {
        console.log(`error: ${error}`);
      });
  }

  onSuccess = (data) => {
    const params = {
      id: data.id,
      status: data.status,
      noDisturb: data.no_disturb,
      isSortChat: data.is_sort_chat,
    };

    this.props.setSetting(params);
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const params = {
      setting: {
        status: this.props.setting.status,
        no_disturb: this.props.setting.noDisturb,
        is_sort_chat: this.props.setting.isSortChat,
      },
    };

    request(`/settings/${this.props.setting.id}`, HTTP_METHODS.PUT, params)
      .then(this.onSuccess)
      .catch((error) => {
        console.log(`error: ${error}`);
      });
  }

  handleInputChange = ({ target }) => {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.props.updateSetting(target.id, value);
  }

  render() {
    return (
      <SettingForm
        handleSubmit={this.handleSubmit}
        handleInputChange={this.handleInputChange}
        setting={this.props.setting}
      />
    );
  }
}

const mapStateToProps = state => ({
  setting: state.setting,
});

const mapDispatchToProps = {
  setSetting: settingActions.setSetting,
  updateSetting: settingActions.updateSetting,
};

Setting.propTypes = {
  setting: PropTypes.shape({
    id: PropTypes.number.isRequired,
    status: PropTypes.string.isRequired,
    noDisturb: PropTypes.bool.isRequired,
    isSortChat: PropTypes.bool.isRequired,
  }).isRequired,
  updateSetting: PropTypes.func.isRequired,
  setSetting: PropTypes.func.isRequired,
};


export default connect(mapStateToProps, mapDispatchToProps)(Setting);
