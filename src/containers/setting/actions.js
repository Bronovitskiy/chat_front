export const settingActions = {
  UPDATE_SETTING: 'UPDATE_SETTING',
  SET_SETTING: 'SET_SETTING',

  updateSetting: (key, value) => ({
    type: settingActions.UPDATE_SETTING,
    payload: {
      value,
      key,
    },
  }),

  setSetting: setting => ({
    type: settingActions.SET_SETTING,
    payload: setting,
  }),
};
