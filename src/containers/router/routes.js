import Login from '../login';
import Contact from '../contact';
import Home from '../home';
import Setting from '../setting';


const routes = [
  {
    path: '/login',
    component: Login,
    default: false,
    authOnly: false,
    layout: false,
  },
  {
    path: '/contact-list',
    component: Contact,
    default: false,
    authOnly: true,
    layout: true,
  },
  {
    path: '/setting',
    component: Setting,
    default: false,
    authOnly: true,
    layout: true,
  },
  {
    path: '/home',
    component: Home,
    default: true,
    authOnly: true,
    layout: true,
  },
];

const navLinksParams = [
  {
    path: '/home',
    content: 'Home',
  },
  {
    path: '/contact-list',
    content: 'Contact List',
  },
  {
    path: '/setting',
    content: 'Setting',
  },
];


export { routes, navLinksParams };
