import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';

import { routes } from './routes';
import Layout from '../../components/layout';
import Storage from '../../utils/storage';


const getRouteWithSubRoutes = (route, index) => {
  const { component: Component } = route;

  if (!route.authOnly) {
    return (
      <Route
        key={index}
        path={route.path}
        component={route.component}
      />
    );
  }

  return (
    <Route
      key={index}
      path={route.path}
      render={props => (
        Storage.getIsLogin() ? (
          <Layout>
            <Component {...props} />
          </Layout>
        )
          : <Redirect to="/login" />
      )}
    />
  );
};

const Router = () => (
  <BrowserRouter>
    <Switch>
      {routes.map((route, index) => getRouteWithSubRoutes(route, index))}
      <Redirect to="/home" />
    </Switch>
  </BrowserRouter>
);


export default Router;
