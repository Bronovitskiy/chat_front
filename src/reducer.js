import { combineReducers } from 'redux';
import home from './containers/home/reducer';
import setting from './containers/setting/reducer';
import contact from './containers/contact/reducer';

export default combineReducers({
  home,
  setting,
  contact,
});
