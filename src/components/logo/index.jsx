import React from 'react';

import logoImg from './chat.svg';

import './style.css';


const Logo = () => (
  <div className="logo">

    <img
      className="logo__image"
      src={logoImg}
      alt="logo"
    />

    <h1 className="logo__text">
      Chat
    </h1>

  </div>
);

export default Logo;
