import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Button = ({ name }) => (
  <button
    type="button"
    className="button"
  >
    {name}
  </button>
);

Button.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Button;
