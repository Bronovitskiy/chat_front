import React from 'react';
import PropTypes from 'prop-types';

import { capitalize } from '../../utils/string-helper';
import { ERROR_CLASS } from './constants';

const Input = ({
  formName,
  id,
  name,
  isValid,
  type,
  placeholder,
  handleInputChange,
  value,
}) => (
  <div className="form__item">

    <label
      className={`${formName}__label`}
      htmlFor={id}
    >
      {capitalize(name)}
    </label>

    <input
      className={`${formName}__input ${isValid ? '' : ERROR_CLASS}`}
      id={id}
      type={type}
      placeholder={placeholder}
      name={name}
      onChange={handleInputChange}
      value={value}
      checked={value}
    />

  </div>
);


Input.propTypes = {
  formName: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  isValid: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
};


export default Input;
