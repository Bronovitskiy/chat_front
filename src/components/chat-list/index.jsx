import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button';

import chatImg from './default-chat.png';

import './style.css';


const ChatList = ({ chats }) => (
  <div className="chats">
    <h3 className="chat__title">
    Your last chats:
    </h3>

    {
      chats.map(item => (
        <div
          key={item.id}
          className="chat"
        >

          <div className="chat__info">

            <img
              className="chat__image"
              src={chatImg}
              alt="logo"
            />

            <div className="chat__name">
              <a
                href="/"
                className="chat__link-to"
              >
                {item.name}
              </a>
            </div>

          </div>

          <div className="chat__action">

            <Button
              name="Delete"
            />

          </div>
        </div>
      ))
      }
  </div>
);

ChatList.propTypes = {
  chats: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
};


export default ChatList;
