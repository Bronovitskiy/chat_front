import React from 'react';
import PropTypes from 'prop-types';

import Input from '../input';

import './style.css';


const LoginForm = ({ handleSubmit, handleInputChange, user }) => (
  <div className="login">
    <h2 className="login__title">
    Log in
    </h2>

    <form className="login__form form" onSubmit={handleSubmit}>
      <Input
        formName="login"
        id="email"
        name="email"
        isValid={user.valid.email}
        type="email"
        placeholder="your-email@gmail.com"
        handleInputChange={handleInputChange}
        value={user.email}
      />

      <Input
        formName="login"
        id="password"
        name="password"
        isValid={user.valid.password}
        placeholder=""
        type="password"
        handleInputChange={handleInputChange}
        value={user.password}
      />

      <Input
        formName="login"
        id="confirmPassword"
        name="confirm password"
        isValid={user.valid.confirmPassword}
        type="password"
        placeholder=""
        handleInputChange={handleInputChange}
        value={user.confirmPassword}
      />

      <input className="login__submit" type="submit" value="Log in" />
    </form>
  </div>
);

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
    confirmPassword: PropTypes.string,
  }).isRequired,
};


export default LoginForm;
