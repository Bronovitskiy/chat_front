import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button';

import contactImg from './default-image.png';

import './style.css';


const ContactList = ({ contacts }) => (
  <div className="contacts">
    <h3 className="contacts__title">
      Your contacts:
    </h3>

    {
      contacts.map(item => (
        <div
          key={item.id}
          className="contact"
        >

          <div className="contact__info">

            <img
              className="contact__image"
              src={contactImg}
              alt="logo"
            />

            <div className="contact__name">
              <a
                href="/"
                className="contact__link-to"
              >
                {item.name}
              </a>
            </div>

          </div>

          <div className="contact__action">

            <Button
              name="Edit"
            />

            <Button
              name="Delete"
            />

          </div>
        </div>
      ))
    }
  </div>
);

ContactList.propTypes = {
  contacts: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
};


export default ContactList;
