import React from 'react';

import Nav from '../nav';
import Logo from '../logo';

import './style.css';

const Header = () => (
  <header className="header">

    <div className="header__logo">
      <Logo />
    </div>

    <div className="header__menu">
      <Nav />
    </div>

  </header>
);


export default Header;
