import React from 'react';
import PropTypes from 'prop-types';

import Header from '../header';

import './style.css';


const Layout = ({ children }) => (
  <div>

    <Header />

    <div className="container">
      {children}
    </div>

  </div>
);

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};


export default Layout;
