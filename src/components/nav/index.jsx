import React from 'react';
import { NavLink } from 'react-router-dom';

import { ACTIVE_MENU_ITEM_CLASS } from './constants';
import { navLinksParams } from '../../containers/router/routes';

import './style.css';


const Nav = () => (
  <nav>
    <ul className="menu">

      {
        navLinksParams.map((item, index) => (
          <li
            key={index} // eslint-disable-line react/no-array-index-key
            className="menu__item"
          >

            <NavLink
              to={item.path}
              className="menu__link"
              activeClassName={ACTIVE_MENU_ITEM_CLASS}
            >
              {item.content}
            </NavLink>

          </li>
        ))
      }

    </ul>
  </nav>
);


export default Nav;
