import React from 'react';
import PropTypes from 'prop-types';

import Input from '../input';

import './style.css';

const Setting = ({ handleSubmit, handleInputChange, setting }) => (
  <div className="setting">
    <h2 className="setting__title">
    Setting
    </h2>

    <form className="setting__form form" onSubmit={handleSubmit}>

      <Input
        formName="setting"
        id="status"
        name="status"
        isValid
        type="text"
        placeholder="at home"
        handleInputChange={handleInputChange}
        value={setting.status}
      />

      <Input
        formName="setting"
        id="noDisturb"
        name="no disturb"
        isValid
        type="checkbox"
        placeholder=""
        handleInputChange={handleInputChange}
        value={setting.noDisturb}
      />

      <Input
        formName="setting"
        id="isSortChat"
        name="sort chat"
        isValid
        type="checkbox"
        placeholder=""
        handleInputChange={handleInputChange}
        value={setting.isSortChat}
      />

      <input className="setting__submit" type="submit" value="save" />
    </form>
  </div>
);

Setting.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  setting: PropTypes.shape({
    status: PropTypes.string.isRequired,
    noDisturb: PropTypes.bool.isRequired,
    isSortChat: PropTypes.bool.isRequired,
  }).isRequired,
};


export default Setting;
