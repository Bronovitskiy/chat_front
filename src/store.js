import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import Reducer from './reducer';

export default function Store(initialState) {
  const logger = createLogger({});

  const store = createStore(
    Reducer,
    initialState,
    applyMiddleware(thunk, logger),
  );

  return store;
}
