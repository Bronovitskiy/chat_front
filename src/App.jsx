import React from 'react';
import { Provider } from 'react-redux';

import Router from './containers/router';
import Store from './store';

import '../node_modules/normalize.css/normalize.css';

const store = Store();

const App = () => (
  <Provider store={store}>
    <div className="app">
      <Router />
    </div>
  </Provider>
);


export default App;
