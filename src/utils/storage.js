import LocalStorageAdapter from './local-storage-adapter';
import { LOCAL_STORAGE } from '../config';


const Storage = {
  getIsLogin: () => (LocalStorageAdapter.getItem(LOCAL_STORAGE.IS_LOGIN) !== null),

  setAuthHeader: object => (
    LocalStorageAdapter.setItem(LOCAL_STORAGE.IS_LOGIN, object)
  ),

  getAuthHeader: () => (
    JSON.parse(LocalStorageAdapter.getItem(LOCAL_STORAGE.IS_LOGIN))
  ),

  removeAuthHeader: () => (
    LocalStorageAdapter.removeItem(LOCAL_STORAGE.IS_LOGIN)
  ),
};

export default Storage;
