const LocalStorageAdapter = {
  getItem: key => (
    localStorage.getItem(key)
  ),
  setItem: (key, value) => (
    localStorage.setItem(key, value)
  ),
  removeItem: key => (
    localStorage.setItem(key, null)
  ),
};

export default LocalStorageAdapter;
