import { BASE_URL, HTTP_METHODS } from '../config';
import Storage from './storage';


const hostWithPath = path => (BASE_URL + path);

const status = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response);
  }

  if (response.status === '401') {
    Storage.removeAuthHeader();
  }

  return Promise.reject(new Error(response.statusText));
};

const json = (response) => {
  if (response.ok) {
    if (response.headers.get('access-token')) {
      const accessToken = response.headers.get('access-token');
      const client = response.headers.get('client');
      const expiry = response.headers.get('expiry');
      const tokenType = response.headers.get('token-type');
      const uid = response.headers.get('uid');

      const authHeader = {
        client,
        expiry,
        uid,
        'access-token': accessToken,
        'token-type': tokenType,
      };

      Storage.setAuthHeader(JSON.stringify(authHeader));
    }

    return response.json();
  }

  return null;
};

const getHeaders = () => ({
  ...Storage.getAuthHeader(),
  Accept: 'application/json, text/plain, */*',
  'Content-Type': 'application/json',
});

const request = (url, method, params) => {
  let fetchParams = {};

  if (method === HTTP_METHODS.GET) {
    fetchParams = {
      method,
      headers: {
        ...getHeaders(),
      },
    };
  } else {
    fetchParams = {
      method,
      headers: {
        ...getHeaders(),
      },
      body: JSON.stringify(params),
    };
  }

  return fetch(hostWithPath(url), {
    ...fetchParams,
  })
    .then(status)
    .then(json)
    .catch((error) => { throw new Error(error); });
};


export default request;
