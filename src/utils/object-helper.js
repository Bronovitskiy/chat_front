import { capitalize } from './string-helper';


const toCamelCase = (object, splitChart = '_') => {
  const newObject = {};
  let newKey;
  let value;

  if (object instanceof Array) {
    return object.map((item) => {
      if (typeof item === 'object') {
        return toCamelCase(item);
      }
      return item;
    });
  }

  Object.keys(object).forEach((originKey) => {
    if (Object.prototype.hasOwnProperty.call(object, originKey)) {
      const nameParts = originKey.split(splitChart).map((item, index) => (
        index > 0 ? capitalize(item) : item
      ));

      newKey = (nameParts.join('') || originKey).toString();
      value = object[originKey];

      if (value instanceof Array || (value !== null && value.constructor === Object)) {
        value = toCamelCase(value);
      }

      newObject[newKey] = value;
    }
  });

  return newObject;
};

const objectToParams = (object, keys) => {
  let params = '';

  Object.keys(object).forEach((item) => {
    if (keys.length && keys.includes(item)) {
      params += `${item}=${object[item]}&`;
    }
  });

  return params;
};


export { toCamelCase, objectToParams };
